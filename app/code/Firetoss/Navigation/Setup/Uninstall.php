<?php

/**
 * Uninstall.php
 *
 * @copyright Copyright © 2017 Firetoss. All rights reserved.
 * @author    sgmadsen@gmail.com
 */
namespace Firetoss\Navigation\Setup;

use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class Uninstall implements UninstallInterface
{
    /**
     * @var array
     */
    protected $tablesToUninstall = [
        MenuSetup::ENTITY_TYPE_CODE . '_entity',
        MenuSetup::ENTITY_TYPE_CODE . '_eav_attribute',
        MenuSetup::ENTITY_TYPE_CODE . '_entity_datetime',
        MenuSetup::ENTITY_TYPE_CODE . '_entity_decimal',
        MenuSetup::ENTITY_TYPE_CODE . '_entity_int',
        MenuSetup::ENTITY_TYPE_CODE . '_entity_text',
        MenuSetup::ENTITY_TYPE_CODE . '_entity_varchar'
    ];

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context) //@codingStandardsIgnoreLine
    {
        $setup->startSetup();

        foreach ($this->tablesToUninstall as $table) {
            if ($setup->tableExists($table)) {
                $setup->getConnection()->dropTable($setup->getTable($table));
            }
        }

        $setup->endSetup();
    }
}

<?php
/**
 * Delete
 *
 * @copyright Copyright © 2017 Firetoss. All rights reserved.
 * @author    sgmadsen@gmail.com
 */
namespace Firetoss\Navigation\Controller\Adminhtml\Menu;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Firetoss\Navigation\Model\MenuFactory;

class Delete extends Action
{
    /** @var menuFactory $objectFactory */
    protected $objectFactory;

    /**
     * @param Context $context
     * @param MenuFactory $objectFactory
     */
    public function __construct(
    Context $context,
    MenuFactory $objectFactory
    ) {
        $this->objectFactory = $objectFactory;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Firetoss_Navigation::menu');
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('entity_id', null);

        try {
            $objectInstance = $this->objectFactory->create()->load($id);
            if ($objectInstance->getId()) {
                $objectInstance->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the record.'));
            } else {
                $this->messageManager->addErrorMessage(__('Record does not exist.'));
            }
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }
        
        return $resultRedirect->setPath('*/*');
    }
}

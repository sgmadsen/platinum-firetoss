<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite0e0cd9a867619f15f2bec29d4179db9
{
    public static $files = array (
        'f174079fc5d55b04ad00371d4bd14e0b' => __DIR__ . '/../..' . '/registration.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {

        }, null, ClassLoader::class);
    }
}
